package net.pltplp.testing.snapshotjava;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.test.web.servlet.ResultMatcher;

import static net.pltplp.testing.snapshotjava.Assert.assertMatchesSnapshot;

/**
 * Snapshot matcher for MockMvc tests.
 */
public final class MockMvcContentSnapshotMatcher {

    private MockMvcContentSnapshotMatcher() {
    }

    /**
     * Test that the MockMvc response body matches the snapshot, without suffit.
     *
     * @return a ResultMatcher that will take care of getting the body and comparing it to the snapshot.
     * @see #contentMatchesSnapshot(String)
     */
    public static ResultMatcher contentMatchesSnapshot() {
        return contentMatchesSnapshot(null);
    }

    /**
     * Test that the MockMvc response body matches the snapshot.
     *
     * @param suffix the snapshot suffix
     * @return a ResultMatcher that will take care of getting the body and comparing it to the snapshot.
     */
    public static ResultMatcher contentMatchesSnapshot(String suffix) {
        return result -> {
            String content = result.getResponse().getContentAsString();
            JsonNode jsonNode = Assert.OBJECT_MAPPER.readTree(content);
            assertMatchesSnapshot(jsonNode, suffix);
        };
    }
}
