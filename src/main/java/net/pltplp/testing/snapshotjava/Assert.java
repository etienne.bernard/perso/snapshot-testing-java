package net.pltplp.testing.snapshotjava;

import com.deblock.jsondiff.DiffGenerator;
import com.deblock.jsondiff.diff.JsonDiff;
import com.deblock.jsondiff.matcher.CompositeJsonMatcher;
import com.deblock.jsondiff.matcher.JsonMatcher;
import com.deblock.jsondiff.matcher.StrictJsonArrayPartialMatcher;
import com.deblock.jsondiff.matcher.StrictJsonObjectPartialMatcher;
import com.deblock.jsondiff.matcher.StrictPrimitivePartialMatcher;
import com.deblock.jsondiff.viewer.PatchDiffViewer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Main class for snapshot asserts.
 */
public final class Assert {

    /**
     * Base path for snapshots.
     */
    public static final String SNAPSHOTS_BASEPATH = "src/test/resources/snapshots";

    private static final int SNAPSHOTS_LINES = 100;

    private static final String PATH_WITHOUT_SUFFIX = "%s/%s/%s.json";
    private static final String PATH_WITH_SUFFIX = "%s/%s/%s-%s.json";

    static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = JsonMapper.builder()
            .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
            .configure(SerializationFeature.INDENT_OUTPUT, true)
            .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
            .serializationInclusion(JsonInclude.Include.NON_NULL)
            .build();
    }

    private static final JsonMatcher JSON_MATCHER = new CompositeJsonMatcher(
        new StrictJsonArrayPartialMatcher(),
        // comparing array using strict mode (array should have same item on same orders)
        new StrictJsonObjectPartialMatcher(),
        // comparing object using strict mode (object should have same properties/value)
        new StrictPrimitivePartialMatcher()
        // comparing primitive types (values should be strictly equals type and value)
    );

    private Assert() {
    }

    /**
     * Test that the <code>object</code> parameter matches the snapshot written on disk. No suffix will be appended.
     *
     * @param object the object you want to test against the snapshot. It will be serialized to JSON using Jackson
     * @see #assertMatchesSnapshot(Object, String)
     */
    public static void assertMatchesSnapshot(Object object) {
        assertMatchesSnapshot(object, null);
    }

    /**
     * Test that the <code>object</code> parameter matches the snapshot written on disk. If the snapshot does not
     * exist yet it will be created.
     * The snapshot will be created in the src/test/resource folder, in a file based on the full test class name
     * (including the package name) and the test method that has made the assertion.
     * Use the <code>suffix</code> parameter to discrimate between multiple calls to <code>assertMatchesSnapshot</code>
     * in the same test method, otherwise the snapshots will be overwritten by the last call.
     *
     * @param object the object you want to test against the snapshot. It will be serialized to JSON using Jackson
     * @param suffix the snapshot suffix
     */
    public static void assertMatchesSnapshot(Object object, String suffix) {
        try {
            Path snapshotPath = getSnapshotPath(suffix);
            if (Files.exists(snapshotPath)) {
                compareObjectToSnapshot(object, snapshotPath);
            } else {
                saveSnapshot(object, snapshotPath);
            }
        } catch (IOException e) {
            throw new SnapshotIOException(e);
        }
    }

    private static void compareObjectToSnapshot(Object object, Path snapshotPath) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(snapshotPath, StandardCharsets.UTF_8)) {
            /* Read the json from snapshot */
            StringBuilder builder = new StringBuilder(SNAPSHOTS_LINES);
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            String expected = builder.toString();
            /* Generate the json for the object */
            String received = OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            /* Generate the diff */
            JsonDiff diff = DiffGenerator.diff(expected, received, JSON_MATCHER);
            if (diff.similarityRate() < 100.0) {
                /* There is some difference - throw assertion error */
                PatchDiffViewer patch = PatchDiffViewer.from(diff);
                throw new AssertionError("\n" + patch.toString());
            }
        }
    }

    private static void saveSnapshot(Object object, Path snapshotPath) throws IOException {
        Files.createDirectories(snapshotPath.getParent());
        Files.createFile(snapshotPath);
        try (BufferedWriter writer = Files.newBufferedWriter(snapshotPath, StandardCharsets.UTF_8)) {
            OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValue(writer, object);
        }
    }

    private static Path getSnapshotPath(String suffix) {
        StackTraceElement callerStackTraceElement = getCallerStackTraceElement();
        String directory = callerStackTraceElement.getClassName().replace('.', '/');
        String baseFilename = callerStackTraceElement.getMethodName();
        String path;
        if (suffix == null) {
            path = String.format(PATH_WITHOUT_SUFFIX, SNAPSHOTS_BASEPATH, directory, baseFilename);
        } else {
            path = String.format(PATH_WITH_SUFFIX, SNAPSHOTS_BASEPATH, directory, baseFilename, suffix);
        }
        return Paths.get(path);
    }

    private static StackTraceElement getCallerStackTraceElement() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        return Stream.of(stackTrace)
            .filter(stackTraceElement -> !stackTraceElement.getMethodName().startsWith("lambda$"))
            .filter(stackTraceElement -> !stackTraceElement.getClassName().startsWith("java.lang."))
            .filter(stackTraceElement -> !stackTraceElement.getClassName().startsWith("org.junit."))
            .filter(stackTraceElement -> !stackTraceElement.getClassName().startsWith("org.springframework.test."))
            .filter(stackTraceElement -> !stackTraceElement.getClassName().startsWith("net.pltplp.testing.snapshotjava."))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Unable to get test method"));
    }

}
