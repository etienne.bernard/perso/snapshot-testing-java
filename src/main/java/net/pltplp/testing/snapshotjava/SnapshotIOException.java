package net.pltplp.testing.snapshotjava;

import java.io.IOException;

/**
 * Dedicated runtime exception.
 */
public class SnapshotIOException extends RuntimeException {

    /**
     * Build from IOException.
     *
     * @param e the IOException
     */
    public SnapshotIOException(IOException e) {
        super(e);
    }
}
