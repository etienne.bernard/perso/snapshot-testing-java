package net.pltplp.testing.snapshotjavatesting;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.nio.file.Files;
import java.nio.file.Paths;

import static net.pltplp.testing.snapshotjava.Assert.SNAPSHOTS_BASEPATH;
import static net.pltplp.testing.snapshotjava.MockMvcContentSnapshotMatcher.contentMatchesSnapshot;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MockMvcContentSnapshotMatcherTest {

    @Test
    void testMockMvcContentSnapshotMatcher() throws Exception {
        MockHttpServletResponse response = mock(MockHttpServletResponse.class);
        when(response.getContentAsString()).thenReturn("[ \"A\", \"B\", \"C\" ]");
        MvcResult result = mock(MvcResult.class);
        when(result.getResponse()).thenReturn(response);

        ResultMatcher resultMatcher = contentMatchesSnapshot();
        resultMatcher.match(result);

        verify(result, times(1)).getResponse();

        // Check that the file was correctly created
        final String JSON_PATH =
            SNAPSHOTS_BASEPATH + "/net/pltplp/testing/snapshotjavatesting/MockMvcContentSnapshotMatcherTest/testMockMvcContentSnapshotMatcher.json";
        assertTrue(Files.exists(Paths.get(JSON_PATH)));
        // Check file content
        String contents = TestUtils.readFileToString(JSON_PATH);
        assertEquals("[ \"A\", \"B\", \"C\" ]", contents);
    }
}
