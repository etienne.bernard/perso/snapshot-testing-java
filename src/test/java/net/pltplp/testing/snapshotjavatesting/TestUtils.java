package net.pltplp.testing.snapshotjavatesting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Collectors;

public final class TestUtils {

    private TestUtils() {
    }

    public static String readFileToString(String filePath) throws FileNotFoundException {
        return new BufferedReader(new FileReader(filePath))
            .lines().collect(Collectors.joining(System.lineSeparator()));
    }
}