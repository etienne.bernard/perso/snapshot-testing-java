package net.pltplp.testing.snapshotjavatesting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static net.pltplp.testing.snapshotjava.Assert.SNAPSHOTS_BASEPATH;
import static net.pltplp.testing.snapshotjava.Assert.assertMatchesSnapshot;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * IMPORTANT: We are not in the same package as the class we are testing, as otherwise we won't be able to extract
 * the snapshot path correctly.
 */
class AssertTest {

    @BeforeEach
    void removeSnapshots() throws IOException {
        Path snapshotsPath = Paths.get(SNAPSHOTS_BASEPATH);
        FileSystemUtils.deleteRecursively(snapshotsPath);
    }

    @Test
    void testAssertArray() throws Exception {
        Object object = new String[] {"A", "B", "C"};
        // Should work directly because there are no snapshots yet
        assertMatchesSnapshot(object);
        // Check that the file was correctly created
        final String JSON_PATH =
            SNAPSHOTS_BASEPATH + "/net/pltplp/testing/snapshotjavatesting/AssertTest/testAssertArray.json";
        assertTrue(Files.exists(Paths.get(JSON_PATH)));
        // Check file content
        String contents = TestUtils.readFileToString(JSON_PATH);
        assertEquals("[ \"A\", \"B\", \"C\" ]", contents);
    }

    @Test
    void testAssertRecord() throws Exception {
        RecordA recordA = new RecordA("Test", 1);
        // Should work directly because there are no snapshots yet
        assertMatchesSnapshot(recordA);
        // Check that the file was correctly created
        final String JSON_PATH =
            SNAPSHOTS_BASEPATH + "/net/pltplp/testing/snapshotjavatesting/AssertTest/testAssertRecord.json";
        assertTrue(Files.exists(Paths.get(JSON_PATH)));
        // Check file content
        String contents = TestUtils.readFileToString(JSON_PATH);
        assertEquals("""
            {
              "a" : 1,
              "b" : "Test"
            }""", contents);
    }

    @Test
    void testAssertClass() throws Exception {
        ClassA object = new ClassA(new RecordA("Essai", 2), true);
        // Should work directly because there are no snapshots yet
        assertMatchesSnapshot(object);
        // Check that the file was correctly created
        final String JSON_PATH =
            SNAPSHOTS_BASEPATH + "/net/pltplp/testing/snapshotjavatesting/AssertTest/testAssertClass.json";
        assertTrue(Files.exists(Paths.get(JSON_PATH)));
        // Check file content
        String contents = TestUtils.readFileToString(JSON_PATH);
        assertEquals("""
            {
              "a" : true,
              "b" : {
                "a" : 2,
                "b" : "Essai"
              }
            }""", contents);
    }

    @Test
    void testAssertWithSuffix() throws Exception {
        String suffix = "suffix";
        Object object = new String[] {"A", "B", "C"};
        // Should work directly because there are no snapshots yet
        assertMatchesSnapshot(object, suffix);
        // Retest if we still match the snapshot
        assertMatchesSnapshot(object, suffix);
    }

    @Test
    void testFailSnapshot() throws Exception {
        Object object = new String[] {"A", "B", "C"};
        // Should work directly because there are no snapshots yet
        assertMatchesSnapshot(object);
        // Change the object
        final Object object2 = new String[] {"A", "C", "B"};
        // Check that the assertion fails
        AssertionError error = assertThrows(AssertionError.class, () -> assertMatchesSnapshot(object2));
        // Check error message
        assertEquals("""
                        
            --- actual
            +++ expected
            @@ @@
             [
               "A",
            -  "C",
            +  "B",
            -  "B"
            +  "C"
             ]""", error.getMessage());
    }


    record RecordA(String b, int a) {
    }


    static class ClassA {

        private RecordA b;
        private boolean a;

        public ClassA(RecordA b, boolean a) {
            this.b = b;
            this.a = a;
        }

        public RecordA getB() {
            return b;
        }

        public boolean isA() {
            return a;
        }
    }
}