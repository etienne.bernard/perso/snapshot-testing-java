# Snapshot testing in Java

This is a very simple library used to do snapshot testing in your java tests. Objects are serialized to JSON using
the Jackson library. See [this document](https://notlaura.com/what-is-a-snapshot-test/) for more information about
what is snapshot testing.

## Basic usage

```java
package my.application;

import static snapshotjava.testing.net.pltplp.Assert.assertMatchesSnapshot;

public class MyTestClass {

    @Test
    void testDoSomething() throws Exception {
        Object result = doSomething();
        assertMatchesSnapshot(object); // Here
    }

}
```

When executing the test for the first time, the `assertMatchesSnapshot` call will
create a serialized version of `result` in the `src/test/resources/my/application/MyTestClass/testDoSomething.json`
file.

You should check that the produced JSON is what you would expect from your test.

_This file should be checked into your version control system._

Next invocations of the test will check that the `result` object stays the same (in its JSON serialized version).

## Multiple snapshots per test method

If you want to have multiple snapshots per test method, you can add a `suffix` parameter
to the `assertMatchesSnapshot` call, and the generated will be named `something-suffix.json` instead of
`something.json`.

## MockMvc implementation

You can also use `contentMatchesSnapshot` for `MockMvc` tests. For example:

```java
import static snapshotjava.testing.net.pltplp.MockMvcContentSnapshotMatcher.contentMatchesSnapshot;

@Test
void getPersons() throws Exception {
    mockMvc.perform(get("/api/something")
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(contentMatchesSnapshot()); // Here
}
```